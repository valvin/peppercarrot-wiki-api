var fs = require('fs');
var LOG_TAG = 'CHARACTERS CTRL'
var CHARACTER_FILE = './data/Characters.md';

characters = new Object();

characters.getCharacters = function(callback){
  fs.readFile(CHARACTER_FILE,'utf8', function(err,data){
    if(err){
      console.log(LOG_TAG + ' : ' + err);
      callback(err);
    }
    else{
      var characters_data = [];

      var reCharacterName = /^# (.*)/mg;
      var match = []
      var characters_index = [];
      while ((match = reCharacterName.exec(data)) !== null) {
        var character = new Object();
        character.name = match[1];
        characters_index.push(match.index);
        //character.description = data.substring(re.index, re.lastindex)
        characters_data.push(character);
      }

      for(i = 0; i < characters_data.length; i++){
        var desc = '';
        if(i < characters_data.length - 1){
          desc = data.substring(characters_index[i], characters_index[i+1]);
        }else{
          desc = data.substring(characters_index[i], data.length - 1);
        }
        var reImage = /\[.*\]\((.*)\)/
        var image = null;
        if(image = reImage.exec(desc)){
          characters_data[i].image = image[1];
        }
        var reAge = /\*\*Age\*\*:\s*(.+?)(?=\n)/
        var age = null;
        if(age = reAge.exec(desc))
          characters_data[i].age = age[1];
        var reMagic = /\*\*Magic\*\*:\s*(.+?)(?=\n)/
        var magic = null;
        if(magic = reMagic.exec(desc))
          characters_data[i].magic = magic[1];
        var rePlace = /\*\*Place\*\*:\s*(.+?)(?=\n)/
        var place = null;
        if (place = rePlace.exec(desc))
          characters_data[i].place = place[1];
        var reDescription = /\*\*Description\*\*:\s*(.+?)(?=\n)/
        var description = null;
        if (description = reDescription.exec(desc))
          characters_data[i].description = description[1];
        var reHistory = /\*\*History\*\*:\s+\n/
        var history = null;
        if(history = reHistory.exec(desc)){
          characters_data[i].history = desc.substring(history.index + history[0].length, desc.length-1);
        }

      }

      callback(characters_data);
    }

  });
}
characters.getMarkdown = function(character){
  var message = '';
  if(character.image) 
    message = '![]('+character.image+')' + '\n\n'
  message += '**' + character.name + '**\n\n';
  if(character.age)
    message += '* Age : ' + character.age + '\n';
  if(character.magic)
    message += '* Magic : ' + character.magic + '\n';
  if(character.place)
    message += '* Place : ' + character.place + '\n';
  if(character.description)
    message += '* Description : ' + character.description + '\n';
  if(character.history)
    message += '* History : ' + character.history + '\n';
  return message;
}
module.exports = characters;
