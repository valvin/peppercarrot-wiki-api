var fs = require('fs');
var LOG_TAG = 'PLACES CTRL'
var PLACE_FILE = './data/Places.md';

places = new Object();

places.getPlaces = function(callback){
  fs.readFile(PLACE_FILE,'utf8', function(err,data){
    if(err){
      console.log(LOG_TAG + ' : ' + err);
      callback(err);
    }
    else{
      var places_data = [];

      var rePlaceName = /^# (.*)/mg;
      var match = []
      var places_index = [];
      while ((match = rePlaceName.exec(data)) !== null) {
        var place = new Object();
        place.name = match[1];
        places_index.push({'index' : match.index, 'matchLength' : match[0].length});
        places_data.push(place);
      }

      for(i = 0; i < places_data.length; i++){
        var desc = '';
        if(i < places_data.length - 1){
          desc = data.substring(places_index[i].index + places_index[i].matchLength, places_index[i+1].index);
        }else{
          desc = data.substring(places_index[i].index + places_index[i].matchLength, data.length - 1);
        }
        var reImage = /\[.*\]\((.*)\)/
        var image = null;
        if(image = reImage.exec(desc)){
          places_data[i].image = image[1];
          places_data[i].description = desc.substring(image.index + image[0].length, desc.length -1);
        }
        else{
          places_data[i].description = desc;
        }
        
      }

      callback(places_data);
    }

  });
}
places.getMarkdown = function(place){
 var message = '';
  if(place.image) 
    message = '![]('+place.image+')' + '\n\n'
  message += '**' + place.name + '**\n\n';
  if(place.description)
    message += '**Description**: ' + place.description + '\n';
  return message;
}
module.exports = places;
