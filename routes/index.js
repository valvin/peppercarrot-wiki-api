var express = require('express');
var router = express.Router();
var _ = require('lodash');
var characters = require('../controllers/characters' );
var places = require('../controllers/places' );

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


/* API Characters */
router.get('/characters', function(req, res, next) {

  characters.getCharacters(function(result){
    res.send(result);
  });
});

router.get('/characters/:name', function(req, res, next) {
  var name = req.params.name;
  console.log('looking for : ' + name);
  characters.getCharacters(function(result){
    var char = _.find(result, function(c){return c.name.toLowerCase() == name.toLowerCase()});
    res.send(char);
  });
});

/* API Places */
router.get('/places', function(req, res, next) {

  places.getPlaces(function(result){
    res.send(result);
  });
});

router.get('/places/:name', function(req, res, next) {
  var name = req.params.name;
  places.getPlaces(function(result){
    var place = _.find(result, function(p){return p.name.toLowerCase() == name.toLowerCase()});
    res.send(place);
  });
});

/* Mattermost Slash */
router.post('/slash/wiki/', function(req, res, next) {
  console.dir(req.body);
  var name = req.body.text;
  var message = '';
  var result = {};
 
  characters.getCharacters(function(characters_info){
    var character_info = _.find(characters_info, function(c){return c.name.toLowerCase() == name.toLowerCase()});
    if(character_info){
      message = characters.getMarkdown(character_info);
      message += '[*Wiki*](https://github.com/Deevad/peppercarrot/wiki)';
      result.text = message;
      //result.response_type = 'in_channel';
      result.response_type = 'ephemeral';
      res.send(result);
    }
    else{
      places.getPlaces(function(places_info){
        var place_info = _.find(places_info, function(p){return p.name.toLowerCase() == name.toLowerCase()});
        if(place_info){
          message = places.getMarkdown(place_info);
        } 
        else{
         message = 'I\'ve not found this character ' + name + '\n';
        }
        message += '[*Wiki*](https://github.com/Deevad/peppercarrot/wiki)';
        
        result.text = message;
        //result.response_type = 'in_channel';
        result.response_type = 'ephemeral';
        res.send(result);
      });
    }


  })

});

module.exports = router;
