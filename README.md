## Pepper&Carrot Wiki API
This is a REST API which exposes wiki data available on github

### Run API

**Initialization**

```
npm install
git submodule init
git submodule update data
```
**Run**

```
npm start
```

**Update Wiki data**
```
git submodule update data
```

**Status**

Now it parses only **characters** and **places**

**Test**
* http://localhost:3000/characters/
* http://localhost:3000/characters/pepper
* Slash Mattermost command :
  * setting in slash command in Mattermost : http://xxxx:3000/slash/wiki (POST argument) 
  * Manual testing : `curl -X POST -d "text=character_name_or_place_name" http://localhost:3000/slash/wiki`
